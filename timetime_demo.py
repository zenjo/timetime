#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

from timetime import TimeTime

# Voici les fonctions qu'on veut comparer:
base = "A"

def base_egal_a():
    "Les fonctions à comparer ne doivent pas avoir d'argument"
    if base == "A":
        b = base

def base_in_a():
    if base in "A":
        b = base



# On créé les objet TimeTime:
f1 = TimeTime(base_egal_a)
f2 = TimeTime(base_in_a)

def demo():
    print("A l'aide de 2 fonctions (base_egal_a et base_in_a), on vérifie ")
    print("si index == \"A\" est plus ou moins rapide que index in \"A\".\n")
    print("Voici les 2 fonctions :")
    print("""
base = "A"

def base_egal_a():
    "Les fonctions à comparer ne doivent pas avoir d'argument"
    if base == "A":
        b = base

def base_in_a():
    if base in "A":
        b = base
""")


    print("On crée donc 2 objet Timetime: f1 = TimeTime(base_egal_a) et ")
    print("f2 = TimeTime(base_in_a). On peut alors faire :\n")
    print("print(f1) puis print(f2) :")
    print(f1)
    print(f2)

    print("\nou print(f1 == f2) :")
    print(f1 == f2)
    print("\nou encore print(f1 < f2) :")
    print(f1 < f2)
    print("\nou enfin print(f1 > f2) :")
    print(f1 > f2)

    print("\n\nAttention, si les fonctions renvoie une impression à l'écran,")
    print("cela peut nuire (éventuellement fortement) à la lisibilité.")

if __name__ == "__main__":
    demo()
    # code_demo()
